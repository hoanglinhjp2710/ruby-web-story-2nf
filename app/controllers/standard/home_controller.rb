module Standard
  class HomeController < DashboardController

    def index;end

    def show
      @user = current_user if user_signed_in?
      @statistic = Home::StatisticSupporter.new
    end

    private
    def parameters
      @params ||= Record::PostParams.new params
    end

    def home_service
      @home_service ||= Record::HomeService.new current_user
    end
  end
end
