class BookMarkController < BaseController
  def create
    @bookmark = bookmark_service.toggle_bookmark!
  end
  private
  def parameters
    @params = Record::PostParams.new params
  end

  def bookmark_service
    @bookmark_service ||= Record::BookmarkService.new(current_user, parameters)
  end
end
