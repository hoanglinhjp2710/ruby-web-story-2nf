class CommentController < BaseController
  def create
    @comment = Comment.new(parameters.configure_comment_params.merge(post_id: params[:post_id], user_id: current_user.id))
    if parameters.configure_comment_params.nil? || parameters.configure_comment_params.blank?
      @comment.save!
      flash[:success] = I18n.t(".comment.success")
      redirect_to root_path
    else
      flash[:error] = I18n.t(".comment.errors")
      redirect_to root_path
    end

  end
  def show
    @comment = Comment.all.where(post_id: params[:post_id])
  end

  private
  def parameters
    @params = Record::CommentParams.new params
  end
end
