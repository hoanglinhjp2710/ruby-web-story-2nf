class DashboardController < BaseController
  layout 'page'
  before_action :search_post

  private
  def search_post
    @q = Post.ransack(params[:q])
    @post = @q.result
    @pagy, @post = pagy(@post, items: 3)
    @comment = Comment.new
  end
end