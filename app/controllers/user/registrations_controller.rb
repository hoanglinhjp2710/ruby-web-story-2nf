# frozen_string_literal: true

class User::RegistrationsController < Devise::RegistrationsController
  # layout 'page', only: [:edit, :update]
  def new
    @user = User.new
  end

  def create
      service.create(parameters.sign_up_params)
      flash[:success] = I18n.t(".devise.registrations.signed_up")
      redirect_to new_user_session_path
    rescue Exception => e
      flash[:error]= "error: #{e.message}"
      redirect_to new_user_registration_path
  end

  def update
      service.update(current_user,parameters.update_params)
      flash[:success] = I18n.t(".devise.registrations.updated")
      redirect_to root_path
    rescue Exception => e
      flash[:error] = "error: #{e.message}"
      redirect_to edit_user_registration_path
  end

  def edit
    super
  end

  private

  def parameters
    @params ||= User::RegistrationParams.new params
  end
  def service
    service ||= User::RegistrationService.new
  end
end
