class Record::BookmarkService < BaseService
  def initialize(user, params)
    @user = user
    @params = params
  end

  def toggle_bookmark!
    bookmark = @user.bookmarks.of_post(@params.post_id).first_or_initialize
    if bookmark.persisted?
      bookmark.destroy!
    else
      bookmark.save!
    end
    bookmark
  end
end