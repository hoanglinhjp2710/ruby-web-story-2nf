# frozen_string_literal: true

class Record::PostService < BaseService
  def initialize(user, params)
    @user = user
    @params = params
  end

  def list_post_active
    @post = Post.active
  end

  def list_confirm
    post = Post.confirm
  end

  def create
    if @user.adminnistrator?
      @post = Post.new(@params.configure_create_params.merge(user_id: @user.id, status_active: 2))
    else
      @post = Post.new(@params.configure_create_params.merge(user_id: @user.id, status_active: 1))
    end

    @post.save!
    @message = @post.active? ? I18n.t("post.create.success_active") :  I18n.t("post.create.success_confirm")
    [@post, @message]
  end

  def find_post
    @post = Post.find(@params.params_id)
  end

  def confirm_post(post)
    if @user.adminnistrator?
      post.status_active = 'active'
      post.save
    end
  end

  def cancel_post(post)
    if @user.adminnistrator?
      post.status_active = 'disactive'
      post.save
    end
  end

  def update(post)
    post.update(@params.update_params)
  end

  def like_destroy
    @like = Like.all.where(post_id: @params.params_id)
    if @like.present?
      @like.each do |i|
        i.destroy
      end
    end
  end

  def bookmark_destroy
    @bookmark = Bookmark.all.where(post_id: @params.params_id)
    if @bookmark.present?
      @bookmark.each do |i|
        i.destroy
      end
    end
  end

  def comment_destroy
    @comment = Comment.all.where(post_id: @params.params_id)
    if @comment.present?
      @comment.each do |i|
        i.destroy
      end
    end
  end


  def posts_bookmarked
    Post.joins(:bookmarks).where(bookmarks: {user_id: @user.id})
  end

  def posts_liked
    Post.joins(:likes).where(likes: {user_id: @user.id})
  end

  def find_post_active_of_current_user
    post = Post.all.where(user_id: @user.id, status_active: 'active')
  end

  def find_post_confirm_of_current_user
    post = Post.all.where(user_id: @user.id, status_active: 'confirm')
  end
end