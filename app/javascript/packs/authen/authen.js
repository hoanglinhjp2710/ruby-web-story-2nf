$(document).ready( function() {
  var check = true;
  var specialChars1 = "<>!#$%^&*()+[]{}?:;|'\"\\,/~`-=";
  var specialChars = "<>@!#$%^&*()_+[]{}?:;|'\"\\,./~`-=";
  var regexEmail = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{3,4})+$/
  var checkFormatEmail = function(email,regexEmail){
    if(regexEmail.test(email)){
      return true;
    }else {
      return false;
    }
  }
  var checkForSpecialChar = function(string,specialChars) {
    for (i = 0; i < specialChars.length; i++) {
      if (string.indexOf(specialChars[i]) > -1) {
        return true
      }
    }
    return false;
  }


  $('#username').keyup( function() {
    let value = $(this).val();
    if(checkForSpecialChar(value,specialChars)){
      $(this).removeClass('is-valid');
      $(this).addClass('is-invalid');
      check = false;
    }else{
      if($(this).val().length > 5 & $(this).val().length < 21){
        $(this).removeClass('is-invalid');
        $(this).addClass('is-valid');
        check = true;
      }else{
        $(this).removeClass('is-valid');
        $(this).addClass('is-invalid');
        check = false;
      }
    }
  });
  $('#email').keyup(function(){
    let email = $(this).val();
    if(checkForSpecialChar(email,specialChars1)){
      $(this).removeClass('is-valid');
      $(this).addClass('is-invalid');
      check = false;
    }else{
      if(checkFormatEmail(email,regexEmail)){
        $(this).removeClass('is-invalid');
        $(this).addClass('is-valid');
        check = true;
      }else{
        $(this).removeClass('is-valid');
        $(this).addClass('is-invalid');
        check = false;
      }
    }
  });


  $('#password').keyup(function(){
    let password = $(this).val();
    if(checkForSpecialChar(password,specialChars)){
      $(this).removeClass('is-valid');
      $(this).addClass('is-invalid');
      check = false;
    }else{
      if($(this).val().length > 5 & $(this).val().length < 41){
        $(this).removeClass('is-invalid');
        $(this).addClass('is-valid');
        check = true;
      }else{
        $(this).removeClass('is-valid');
        $(this).addClass('is-invalid');
        check = false;
      }
    }
  });

  $('#confirm_password').keyup(function(){
    let confirm_password = $(this).val();
    let password = $('#password').val();
    if (password === confirm_password){
      $(this).removeClass('is-invalid');
      $(this).addClass('is-valid');
      check = true;
    }else{
      $(this).removeClass('is-valid');
      $(this).addClass('is-invalid');
      check = false;
    }
  });
});