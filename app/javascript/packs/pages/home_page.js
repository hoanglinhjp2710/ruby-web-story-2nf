$(document).ready(function(){
  var check = true;
  $('#title').keyup(function(){
    let title = $(this).val();
    if (title !=''){
      $(this).removeClass('is-invalid');
      $(this).addClass('is-valid');
      check = true;
    }else{
      $(this).removeClass('is-valid');
      $(this).addClass('is-invalid');
      check = false;
    }
  });
  $('#content').keyup(function(){
    let content = $(this).val();
    if (content !=''){
      $(this).removeClass('is-invalid');
      $(this).addClass('is-valid');
      check = true;
    }else{
      $(this).removeClass('is-valid');
      $(this).addClass('is-invalid');
      check = false;
    }
  });
  $('#cover_image').mouseout(function(){
    let cover_image = document.getElementById("cover_image").files;
    if (cover_image.length !=0){
      $(this).removeClass('is-invalid');
      $(this).addClass('is-valid');
      check = true;
    }else{
      $(this).removeClass('is-valid');
      $(this).addClass('is-invalid');
      check = false;
    }
  });
  // $('#language').change(function(){
  //   if(document.getElementById('language').val = "en"){
  //     console.log('a');
  //   }
  // })
});