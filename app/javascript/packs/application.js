require("@rails/ujs").start()
// require("turbolinks").start()
require("@rails/activestorage").start()
require("channels") // ActionCable
require("jquery")
window.jQuery = $
window.$ = $
require("bootstrap")
import 'bootstrap'
import '../stylesheets/application.scss'
import './authen/authen'
import './pages/home_page'
import './pages/profile'
require("./plugins/fontawesome")
//import toastr
import toastr from 'toastr';

toastr.options = {
  'closeButton': true,
  'positionClass': 'toast-top-right',
  'preventDuplicates': false,
  'onclick': null,
  'showDuration': '1000',
  'hideDuration': '0',
  'timeOut': '1000',
  'showEasing': 'swing',
  'hideEasing': 'linear',
  'showMethod': 'fadeIn',
  'hideMethod': 'fadeOut'
}
window.toastr = toastr
